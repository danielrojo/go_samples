package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHelloWorld(t *testing.T) {
	main()

	// check if the output is "Hello, world."
	expected := "Hello, world."

	actual := helloWorld()

	if actual != expected {
		t.Errorf("TestMain: Test failed, expected: '%s', got:  '%s'", expected, actual)
	}
}

func TestSumFunction(t *testing.T) {
	// check if the output is 3
	expected := 3

	actual := sum(1, 2)
	assert.Equal(t, expected, actual, "TestMain: Test failed, expected: '%d', got:  '%d'", expected, actual)

}
