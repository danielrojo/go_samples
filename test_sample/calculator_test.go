package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// object calculator
var calculator Calculator

func TestSumCalculator(t *testing.T) {
	calculator.SetNum1(1)
	calculator.SetNum2(2)
	calculator.Operate("+")
	calculator.Calculate()
	expected := float32(3)
	actual := calculator.GetResult()
	if actual != expected {
		assert.Fail(t, "TestSumCalculator: Test failed, expected: '%f', got:  '%f'", expected, actual)
	}
}

func TestDivideByZero(t *testing.T) {
	calculator.SetNum1(2)
	calculator.SetNum2(0)
	calculator.Operate("/")
	expected := "cannot divide by zero"
	actual := calculator.Calculate().Error()
	// actual is not nil
	assert.NotNil(t, actual, "TestDivideByZero: Test failed, expected: '%s', got:  '%s'", expected, actual)
	assert.Equal(t, expected, actual, "TestDivideByZero: Test failed, expected: '%s', got:  '%s'", expected, actual)

}

func TestDivideZeroByZero(t *testing.T) {
	calculator.SetNum1(0)
	calculator.SetNum2(0)
	calculator.Operate("/")
	expected := "0/0 is a mathematical indetermination"
	actual := calculator.Calculate().Error()
	// actual is not nil
	assert.NotNil(t, actual, "TestDivideZeroByZero: Test failed, expected: '%s', got:  '%s'", expected, actual)
	assert.Equal(t, expected, actual, "TestDivideZeroByZero: Test failed, expected: '%s', got:  '%s'", expected, actual)
}
