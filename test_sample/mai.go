package main

import (
	"fmt"
)

func helloWorld() string {
	return "Hello, world."
}

func main() {
	fmt.Println("Hello, world.")
}

func sum(a int, b int) int {
	return a + b
}
