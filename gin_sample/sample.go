package main

import (
	"fmt"
	"gin_sample/calculator"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
	"github.com/tidwall/gjson"
)

// function to cast from string to float32, returns error if the cast fails
func toFloat32(s string) (float32, error) {
	var f float32
	_, err := fmt.Sscanf(s, "%f", &f)
	return f, err
}

// function to decode the query parameters from url
func decodeQuery(op string) (string, error) {
	if op == "+" || op == "-" || op == "*" || op == "/" {
		return op, nil
	} else {
		return "", fmt.Errorf("Invalid operator")
	}
}

func main() {
	r := gin.Default()
	r.Static("/assets", "./assets")
	v1 := r.Group("api/v1")
	{
		v1.GET("/calculator", getFunction)
		v1.PUT("/calculator", putFunction)
		v1.GET("/apod", getApodFunction)
		v1.GET("/beers", getBeersFunction)
		v1.GET("/beers/:id", getBeerFunction)
		v1.POST("/beers", postBeerFunction)
		v1.PUT("/beers/:id", putFunction)
		v1.DELETE("/beers/:id", putFunction)
	}
	// 404
	r.NoRoute(errorFunction)
	r.Run(":8080")
}

type Apod struct {
	Date           string
	Explanation    string
	Hdurl          string
	MediaType      string
	ServiceVersion string
	Title          string
	Url            string
}

type Beer struct {
	Id          int
	Name        string
	Tagline     string
	FirstBrewed string
	Description string
	ImageUrl    string
	Abv         float32
	Ibu         float32
}

func deleteBeerFunction(c *gin.Context) {
	id, err := toFloat32(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid id"})
		return
	}
	response(c, http.StatusOK, gin.H{"result": "Beer deleted"}, "json")
}

func putBeerFunction(c *gin.Context) {
	var json struct {
		Name        string  `json:"name" binding:"required"`
		Tagline     string  `json:"tagline" binding:"required"`
		FirstBrewed string  `json:"first_brewed" binding:"required"`
		Description string  `json:"description" binding:"required"`
		ImageUrl    string  `json:"image_url" binding:"required"`
		Abv         float32 `json:"abv" binding:"required"`
		Ibu         float32 `json:"ibu" binding:"required"`
	}
	// echo the input
	if c.BindJSON(&json) == nil {
		beer := Beer{Name: json.Name, Tagline: json.Tagline, FirstBrewed: json.FirstBrewed, Description: json.Description, ImageUrl: json.ImageUrl, Abv: json.Abv, Ibu: json.Ibu}
		response(c, http.StatusOK, gin.H{"result": beer}, "json")
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid json format"})
	}
}

func postBeerFunction(c *gin.Context) {
	var json struct {
		Name        string  `json:"name" binding:"required"`
		Tagline     string  `json:"tagline" binding:"required"`
		FirstBrewed string  `json:"first_brewed" binding:"required"`
		Description string  `json:"description" binding:"required"`
		ImageUrl    string  `json:"image_url" binding:"required"`
		Abv         float32 `json:"abv" binding:"required"`
		Ibu         float32 `json:"ibu" binding:"required"`
	}
	// echo the input
	if c.BindJSON(&json) == nil {
		beer := Beer{Name: json.Name, Tagline: json.Tagline, FirstBrewed: json.FirstBrewed, Description: json.Description, ImageUrl: json.ImageUrl, Abv: json.Abv, Ibu: json.Ibu}
		response(c, http.StatusCreated, gin.H{"result": beer}, "json")
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid json format"})
	}
}

func getBeerFunction(c *gin.Context) {
	// get path parameter id
	id, err := toFloat32(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid id"})
		return
	}
	beers := getBeersFromApi(c)
}

func getBeersFromApi(c *gin.Context) []Beer {
	client := resty.New()
	resp, err := client.R().Get("https://api.punkapi.com/v2/beers/" + fmt.Sprint(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal server error"})
		return nil
	} else {
		// get complete response body as object
		gj := gjson.Parse(resp.String()).Array()
		// get struct array of beers
		beers := processBeers(gj)
		if len(beers) == 0 {
			c.JSON(http.StatusNotFound, gin.H{"error": "Not found"})
			return nil
		}
		return beers
	}
}

func getBeersFunction(c *gin.Context) {
	client := resty.New()

	resp, err := client.R().Get("https://api.punkapi.com/v2/beers")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Internal server error"})
		return
	} else {
		abv_gt, abv_lt := processPameters(c)
		// get complete array of beers
		gj := gjson.Parse(resp.String()).Array()
		// get struct array of beers
		beers := processBeers(gj)
		// filter beers by abv_gt and abv_lt
		filteredBeers := filterBeers(beers, abv_gt, abv_lt)
		response(c, http.StatusOK, gin.H{"results": filteredBeers}, "json")
		// response(c, http.StatusOK, gin.H{"result": gj}, "json")
	}
}

func filterBeers(beers []Beer, abv_gt float32, abv_lt float32) []Beer {
	var filteredBeers []Beer
	for _, beer := range beers {
		if beer.Abv >= abv_gt && beer.Abv <= abv_lt {
			filteredBeers = append(filteredBeers, beer)
		}
	}
	return filteredBeers
}

func processBeers(gj []gjson.Result) []Beer {
	var beers []Beer
	for _, beer := range gj {
		b := Beer{beer.Index, beer.Get("name").String(), beer.Get("tagline").String(), beer.Get("first_brewed").String(), beer.Get("description").String(), beer.Get("image_url").String(), float32(beer.Get("abv").Float()), float32(beer.Get("ibu").Float())}
		beers = append(beers, b)
	}
	return beers
}

func processPameters(c *gin.Context) (abv_gt float32, abv_lt float32) {
	// get query parameters abv_gt, abv_lt
	abv_gt, err1 := toFloat32(c.DefaultQuery("abv_gt", "0"))
	if err1 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid abv_gt"})
		return
	}
	abv_lt, err2 := toFloat32(c.DefaultQuery("abv_lt", "100"))
	if err2 != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid abv_lt"})
		return
	}
	// check valid range for abv_gt and abv_lt
	if abv_gt > abv_lt {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid range for abv_gt and abv_lt"})
		return
	}
	// check valid values between 0 and 100 for abv_gt and abv_lt
	if abv_gt < 0 || abv_gt > 100 || abv_lt < 0 || abv_lt > 100 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid range for abv_gt and abv_lt"})
		return
	}
	return abv_gt, abv_lt
}

func getApodFunction(c *gin.Context) {
	client := resty.New()

	resp, err := client.R().
		SetHeader("Accept", "application/json").
		Get("https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY")
	if err != nil {
		c.JSON(500, gin.H{"error": "Internal server error"})
		return
	} else {
		// get complete response body as object
		gjsonObj := Apod{
			Date:           gjson.Get(string(resp.Body()), "date").String(),
			Explanation:    gjson.Get(string(resp.Body()), "explanation").String(),
			Hdurl:          gjson.Get(string(resp.Body()), "hdurl").String(),
			MediaType:      gjson.Get(string(resp.Body()), "media_type").String(),
			ServiceVersion: gjson.Get(string(resp.Body()), "service_version").String(),
			Title:          gjson.Get(string(resp.Body()), "title").String(),
			Url:            gjson.Get(string(resp.Body()), "url").String(),
		}
		response(c, 200, gin.H{"result": gjsonObj}, "json")
	}
	fmt.Println(resp)
}

// function to handle the PUT request
// from json body {num1: float, num2: float, op: string, format: string}
func putFunction(c *gin.Context) {
	var json struct {
		num_1  float32
		num_2  float32
		op     string
		format string
	}
	// echo the input
	if c.BindJSON(&json) == nil {
		if isValidFormat(json.format) == false {
			c.JSON(400, gin.H{"error": "Invalid format: " + json.format,
				"valid_formats": "json, xml, yaml"})
			return
		}
		if json.num_1 == 0 || json.num_2 == 0 {
			c.JSON(400, gin.H{"error": "Invalid num1 or num2"})
			return
		}
		op, errorOp := decodeQuery(json.op)
		if errorOp != nil {
			c.JSON(400, gin.H{"error": "Invalid operator"})
			return
		}
		calculator := calculator.Calculator{Num2: json.num_2, Num1: json.num_1, Op: op, Result: 0}
		resError := calculator.Calculate()
		if resError != nil {
			c.JSON(400, gin.H{"error": resError.Error()})
			return
		}
		response(c, 200, gin.H{"result": calculator}, json.format)
	} else {
		c.JSON(400, gin.H{"error": "Invalid json format"})
	}
}

func errorFunction(c *gin.Context) {
	c.JSON(404, gin.H{"error": "Not found",
		"message": "The specified route was not found",
		"hint":    "Valid urls are: /api/v1/calculator"})
}

func isValidFormat(format string) bool {
	return format == "json" || format == "xml" || format == "yaml"
}

func getFunction(c *gin.Context) {
	format := c.DefaultQuery("format", "json")
	if isValidFormat(format) == false {
		response(c, 400, gin.H{"error": "Invalid format: " + format,
			"valid_formats": "json, xml, yaml"}, format)
		return
	}
	num1, error1 := toFloat32(c.DefaultQuery("num1", "1"))
	if error1 != nil {
		//c.JSON(400, gin.H{"error": "Invalid num1"})
		response(c, 400, gin.H{"error": "Invalid num1"}, format)
		return
	}
	num2, error2 := toFloat32(c.DefaultQuery("num2", "1"))
	if error2 != nil {
		response(c, 400, gin.H{"error": "Invalid num2"}, format)
		return
	}
	op, errorOp := decodeQuery(c.DefaultQuery("op", "+"))
	if errorOp != nil {
		response(c, 400, gin.H{"error": "Invalid operator"}, format)
		return
	}

	calculator := calculator.Calculator{Num2: num2, Num1: num1, Op: op, Result: 0}
	resError := calculator.Calculate()
	if resError != nil {

		response(c, 400, gin.H{"error": resError.Error()}, format)
		return
	}
	// result := calculator.GetResult()
	response(c, 200, gin.H{"result": calculator}, format)
}

func response(c *gin.Context, code int, data gin.H, format string) {
	switch format {
	case "json":
		c.JSON(code, data)
	case "xml":
		c.XML(code, data)
	case "yaml":
		c.YAML(code, data)
	default:
		c.JSON(code, data)
	}
}
