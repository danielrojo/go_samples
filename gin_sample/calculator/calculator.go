package calculator

import (
	"errors"
)

// Calculator struct
type Calculator struct {
	Num1   float32
	Num2   float32
	Op     string
	Result float32
}

// calculate method
func (c *Calculator) Calculate() error {
	switch c.Op {
	case "+":
		c.Result = c.Num1 + c.Num2
		return nil
	case "-":
		c.Result = c.Num1 - c.Num2
		return nil
	case "*":
		c.Result = c.Num1 * c.Num2
		return nil
	case "/":
		if c.Num2 == 0 {
			return errors.New("cannot divide by zero")
		}
		c.Result = c.Num1 / c.Num2
		return nil
	}
	return errors.New("invalid operator")

}

// GetResult method
func (c *Calculator) GetResult() float32 {
	return c.Result
}

// SetNum1 method
func (c *Calculator) SetNum1(Num1 float32) {
	c.Num1 = Num1
}

// SetNum2 method
func (c *Calculator) SetNum2(Num2 float32) {
	c.Num2 = Num2
}

// Op method
func (c *Calculator) Operate(Op string) error {
	if Op == "+" || Op == "-" || Op == "*" || Op == "/" {
		c.Op = Op
		return nil
	} else {
		return errors.New("invalid operator")
	}
}

// interface for calculator
type ICalculator interface {
	calculate() error
	GetResult() float32
	SetNum1(Num1 float32)
	SetNum2(Num2 float32)
	Op(Op string) error
}
